
import sys
import glob, subprocess
import os, os.path

def run_batch_blat(query, prefix, blatpath='/usr/local/bin/blat', outdir='blatoutput'):
    """BLATs all files with a given prefix against given database.
    
    This function BLATs the given query file to every file in the current directory that  begins
    with the specified prefix.
    """
    files = [i for i in glob.glob(prefix+'*') if '.psl' not in i]
    for each in files:
        outfile = each + '.psl'
        outpath = os.path.join(outdir, outfile)
        if os.path.exists(outpath):   # skip over files that have already been generated
            continue
        cmd = '%s %s %s %s' % (blatpath, each, query, outpath)
        print cmd
        output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).communicate()[0]
        print output


if __name__ == "__main__":
    query = sys.argv[1]
    prefix = sys.argv[2]

    try:
        os.makedirs('blatoutput')
    except os.error:
        pass

    run_batch_blat(query, prefix)