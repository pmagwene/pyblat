"""
Simply run blat as normal

blat - Standalone BLAT sequence search command line tool

usage:
   blat database query [-ooc=11.ooc] output.psl

where:

   database is either a .fa file, a .nib file, or a list of .fa or .nib
   files, query is similarly a .fa, .nib, or list of .fa or .nib files
   -ooc=11.ooc tells the program to load over-occurring 11-mers from
               and external file.  This will increase the speed
               by a factor of 40 in many cases, but is not required

   output.psl is where to put the output.


where database is the genome assembly of interest and query is a fasta file with the sequences of interest.   


"""

import subprocess, sys, os
import csv
import glob

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq


#------------------------------------------------------------------------------
# generic utility functions

def fasta_asdict(fname):
    return SeqIO.to_dict(SeqIO.parse(fname, 'fasta'))

def write_seqrecs(seqrecs, fname):
    SeqIO.write(seqrecs, fname, 'fasta')


#------------------------------------------------------------------------------

commastr_to_int_tuple = lambda x: [int(i) for i in x.split(',') if len(i)]

psl_cols = ['match','mismatch','rep_match', 'Ns','Q_gap_count','Q_gap_bases', 'T_gap_count','T_gap_bases',
        'strand', 'Q_name', 'Q_size', 'Q_start', 'Q_end', 'T_name', 'T_size', 'T_start', 'T_end',
        'block_count', 'block_sizes', 'qStarts', 'tStarts']

pslx_cols = psl_cols + ['Q_seq', 'T_seq']  

psl_convert = {'match':int,'mismatch':int, 'rep_match':int, 'Ns':int,'Q_gap_count':int,'Q_gap_bases':int, 'T_gap_count':int,'T_gap_bases':int,
        'strand':str, 'Q_name':str, 'Q_size':int, 'Q_start':int, 'Q_end':int, 'T_name':str, 'T_size':int, 'T_start':int, 'T_end':int,
        'block_count':int, 'block_sizes':commastr_to_int_tuple, 'qStarts':commastr_to_int_tuple, 'tStarts':commastr_to_int_tuple, 
        'Q_seq':str, 'T_seq':str}

def convert_pslstrdict(d, convertdict=psl_convert):
    return dict((k, convertdict[k](v)) for (k,v) in d.iteritems())
      


#------------------------------------------------------------------------------


class PslMatch(dict):
    def __init__(self, matchdict):
        self.update(dict((k, psl_convert[k](v)) for (k,v) in matchdict.iteritems()))
        for (k,v) in matchdict.iteritems():
            self.__setattr__(k, psl_convert[k](v))

    def to_seqrecord(self, fastadict):
        """ Blat record, FASTA dictionary --> extract respective sequence from FASTA dictionary.

        Blat record as returned by parse_blat_psl -- i.e. one of the values in mdict
        FASTA dictionary represents a dictionary of target sequences (i.e. a fasta dictionary created
            from the FASTA database that the BLAT was run against.
        """
        qname = self.Q_name.strip(',')
        tstart = self.T_start
        tend = self.T_end
        tname = self.T_name.strip(',')
        strand = self.strand
        seqrec = fastadict[tname]    

        if seqrec is None:
            return None

        sid = qname
        sname = "%s %s" % (qname, tname)
        sdescription = "%s %d-%d (strand: %s)" % (tname, tstart, tend, strand)
        if strand == '-':   # if match is reverse complement
            s = seqrec.seq[tstart:tend]
            s = s.reverse_complement()
        else:
            s = seqrec.seq[tstart:tend]
        return SeqRecord(s, id = sid, name = sname, description = sdescription)




def parse_psl(fname, skiprows=5, fieldnames=psl_cols):
    """Parse a BLAT psl file.
    
    When there are multiple hits, find the best hit.
    
    Returns four dictionaries:
        1. allhits
        2. unique hits
        3. queries with multiple hits
        4. besthits (from multiple hits)
    
    """

    lines = open(fname, 'r').readlines()
    rdr = csv.DictReader(lines[skiprows:], fieldnames=fieldnames, delimiter="\t", skipinitialspace=True)    

    hitdict = dict()
    [hitdict.setdefault(row['Q_name'], []).append(row) for row in rdr] 
    uniques = dict()
    multis = dict()

    [uniques.setdefault(k, v[0]) if len(v) == 1 else multis.setdefault(k,v) for (k,v) in hitdict.iteritems()]
    bestmatches = dict((k, max(v, key=lambda x: float(x['match'])/float(x['Q_size']))) for (k,v) in multis.iteritems())

    return hitdict, uniques, multis, bestmatches




def parse_pslx(fname, skiprows=5):
    return parse_blat_psl(fname, skiprows, pslx_cols)

    
#------------------------------------------------------------------------------


def blatrec_to_seqrec(blatrec, fastadict):
    """ Blat record, FASTA dictionary --> extract respective sequence from FASTA dictionary.

    Blat record as returned by parse_blat_psl -- i.e. one of the values in mdict
    FASTA dictionary represents a dictionary of target sequences (i.e. a fasta dictionary created
        from the FASTA database that the BLAT was run against.
    """
    qname = blatrec['Q_name']
    tstart = int(blatrec['T_start'])
    tend = int(blatrec['T_end'])
    tname = blatrec['T_name']
    strand = blatrec['strand']
    seqrec = fastadict[tname]    

    if seqrec is None:
        return None

    hdr = "%s %s,%d-%d(strand: %s) %s" % (qname, tname, tstart, tend, strand, seqrec.id)
    if strand == '-':   # if match is reverse complement
        s = seqrec.seq[tstart:tend]
        s = s.reverse_complement()
    else:
        s = seqrec.seq[tstart:tend]
     
    return SeqRecord(s, hdr)    

def getblatrec_fromfile(blatrec, fastafile):
    fastadict = fasta_asdict(fastafile)
    return blatrec_to_seqrec(blatrec, fastadict)



def extract_blat_targets(targetfile, blatdict):
    """Target sequence file, parsed Blat PSL dict --> dictionary from query names to sequence hits on target.
    
    Takes a dictionary of Blat info as returned from parse_blat_psl file and finds the hits in the
    target sequence file. Returns a dictionary mapping query names to FastaRecords where each FastaRecord
    is the hit in the target file.        
    """
    
    fastadict = fasta_asdict(targetfile)
    hitdict ={}
    for qname in blatdict:
        hitdict[qname] = blatrec_to_seqrec(blatdict[qname], fastadict)
    return hitdict   
    

def parse_and_extract_targets(pslfile, targetfile, outfilename, minmultisize = 1000, minmultifrac=0.85):
    """ pslfile, targetfile, outfilename --> file with all target hits (as FastaRecords).
    """
    h, u, m, b = parse_blat_psl(pslfile)
    u.update(b)
    matchdict = extract_blat_targets(targetfile, u)
    ofile = open(outfilename, 'w')
    SeqIO.write(matchdict.values(), "fasta")

    


def extract_gene(targetfile, pslfile, genename):
    """ Target sequence file, PSL file from Blat Query, Gene Name -> hit sequence in the target file. 
    
    Returned values are strings rather than FastaRecords  
    """
    h, u, m, b= parse_blat_psl(pslfile)
    u.update(b)
    blatdict = u
    seqdict = fasta_asdict(targetfile)
    item = blatdict[genename]
    tstart = int(item['T_start'])
    tend = int(item['T_end'])
    tname = item['T_name']
    strand = item['strand']
    seq = seqdict[tname]    
    
    if strand == '-':   # if match is reverse complement
        s = seqrec.seq[tstart:tend]
        s = s.reverse_complement()
    else:
        s = seqrec.seq[tstart:tend]  
    return s   
    

def extract_upstream(targetfile, pslfile, genename, numnuc=1000):
    """ Like extract_gene but pulls out upstream sequences.
    
    Return numnuc bp of upstream sequence.
    """    
    h, u, m, b= parse_blat_psl(pslfile)
    u.update(b)
    blatdict = u
    seqdict = fasta_asdict(targetfile)
    item = blatdict[genename]
    tstart = int(item['T_start'])
    tend = int(item['T_end'])
    tname = item['T_name']
    strand = item['strand']
    seq = seqdict[tname]    
    
    #print tstart, tend, strand
    
    if strand == '-':   # if match is reverse complement
        s = Seq(seq.sequence[tend:tend+numnuc])
        s = s.reverse_complement()
    else:
        s = seq.sequence[tstart-numnuc:tstart]    
    return s     
    

def extract_downstream(targetfile, pslfile, genename, numnuc=1000):
    """ Like extract_gene but pulls out downstream sequences.
    
    Return numnuc bp of downstream sequence.
    """    
    h, u, m, b= parse_blat_psl(pslfile)
    u.update(b)
    blatdict = u
    seqdict = fasta_asdict(targetfile)
    item = blatdict[genename]
    tstart = int(item['T_start'])
    tend = int(item['T_end'])
    tname = item['T_name']
    strand = item['strand']
    seq = seqdict[tname]    
    
    #print tstart, tend, strand
    
    if strand == '-':   # if match is reverse complement
        s = Seq(seq.sequence[tstart-numnuc:tstart])
        s = s.reverse_complement()
    else:
        s = seq.sequence[tstart:tstart+numnuc]    
    return s        
    

def extract_gene_plus_flanking(targetfile, pslfile, genename, up=1000, down=1000):
    """Extracts gene plus specified amount of upstream and downstream sequence."""
    u = extract_upstream(targetfile, pslfile, genename, up)
    d = extract_downstream(targetfile, pslfile, genename, down)    
    g = extract_gene(targetfile, pslfile, genename)
    
    s = u + g + d
    return s    


#------------------------------------------------------------------------------
        

def run_batch_blat(queryfile, ext='.fa', blatpath='/usr/local/bin/blat'):
    """BLATs given query file to all files with a given extension.
    
    This function BLATs the given query file to every file in the current directory that ends 
    in the specificed extension. The default filename extension is '.fa'.
    """
    files = glob.glob('*'+ext)
    for each in files:
        outfile = each[:-len(ext)] + '.psl'
        cmd = '%s %s %s %s' % (blatpath, each, queryfile, outfile)
        print cmd
        output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).communicate()[0]
        print output

#------------------------------------------------------------------------------
# parallels of extract_gene, extract_upstream, extract_flanking but
# for retrieving corresponding sequences from multiple files in batch


def multi_extract_gene(gene, pslext='.psl', targetext='.fa'): 
    records = []
    files = glob.glob('*'+pslext)    
    for pslfile in files:        
        fileroot = pslfile[:-len(pslext)]
        targetfile = fileroot + targetext
        h, u, m, b= parse_blat_psl(pslfile)
        u.update(b)
        matchdict = extract_blat_targets(targetfile, u)
        try:
            rec = matchdict[gene]
        except KeyError:
            print "%s not found in %s" % (gene, targetfile)
            continue
        rec.header = fileroot
        records.append(rec)
    return records        
        
def multi_extract_upstream(gene, pslext='.psl', targetext='.fa'): 
    records = []
    files = glob.glob('*'+pslext)    
    for pslfile in files:                
        fileroot = pslfile[:-len(pslext)]
        targetfile = fileroot + targetext
        s = extract_upstream(targetfile, pslfile, gene)        
        rec = fasta.FastaRecord('temp',s)
        rec.header = fileroot
        records.append(rec)
    return records         

def multi_extract_flanking(gene, pslext='.psl', targetext='.fa', up=1000, down=1000):
    records = []
    files = glob.glob('*'+pslext)    
    for pslfile in files:                
        fileroot = pslfile[:-len(pslext)]
        targetfile = fileroot + targetext
        try:
            s = extract_gene_plus_flanking(targetfile, pslfile, gene, up, down)
        except KeyError:
            print "%s not found in %s" % (gene, targetfile)
            continue
        rec = fasta.FastaRecord('temp',s)
        rec.header = fileroot
        records.append(rec)
    return records         


        
    
    




#------------------------------------------------------------------------------#

